#ifndef UNTITLED1_NODE_H
#define UNTITLED1_NODE_H

// Yes, this is a very tiny node. Everything is accessibly publicly, without accessors or mutators

class node {
public:
    node *left, *right;
    int data;
    unsigned frequency;
    explicit node(int data) : data(data), frequency(1), next(nullptr), prev(nullptr) {}
};


#endif //UNTITLED1_NODE_H
